;;; init-org.el --- Oppsett for org-mode


(require 'org)


;;; Commentary:
;;

;;; Code:
(setq-default org-export-publishing-directory "public")

(provide 'init)

;;; init-org.el ends here
